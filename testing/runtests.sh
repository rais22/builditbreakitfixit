#!/bin/bash
# BUILD
docker build -t rs-bbf:1.0 --build-arg CACHEBUST=$(date +%s) . > /dev/null 2>&1
# RUN TESTS
docker container run --name rs-bbf -p 5000:80 rs-bbf:1.0
# CLEAN
docker container stop rs-bbf > /dev/null 2>&1
docker rm $(docker ps -aq) > /dev/null 2>&1
